## WAF配置文件
1. 下载WAF规则配置文件，[waf.xml](https://gitee.com/haison/jWAF/blob/master/sample/waf.xml) 
1. 将waf.xml放入/WEB-INF目录下

## web.xml修改
在web.xml中增加如下的配置
```xml
<filter>
  	<filter-name>security-filter</filter-name>
  	<filter-class>com.gitee.seawaf.filters.WafFilter</filter-class>
</filter>
<filter-mapping>
  	<filter-name>security-filter</filter-name>
  	<url-pattern>/*</url-pattern>
</filter-mapping>
<listener>
  	<listener-class>com.gitee.seawaf.listeners.WafSessionListener</listener-class>
</listener>
<listener>
  	<listener-class>com.gitee.seawaf.listeners.WafSessionAttrListener</listener-class>
</listener>
<listener>
	<listener-class>net.sf.ehcache.constructs.web.ShutdownListener</listener-class>
</listener>
 ```

## JAR包加载
* 从发行版中下载最新的版本，[release](https://gitee.com/haison/jWAF/releases)
* 从maven中央仓库加载
```xml
<dependency>
    <groupId>com.gitee.haison</groupId>
    <artifactId>jWAF</artifactId>
    <version>1.0.1</version>
</dependency>
```

## 配置说明
```xml
<?xml version="1.0" encoding="UTF-8"?>
<waf>
	<application>
		<id>EHR</id> <!-- 无业务含义，当发送警告信息给管理员时需提供本信息-->
		<name>Human Resource Management System</name> <!-- 无业务含义，当发送警告信息给管理员时需提供本信息 -->
		<ip>192.168.1.131</ip> <!-- 无业务含义，当发送警告信息给管理员时需提供本信息 -->
		<port>8080</port> <!-- 无业务含义，当发送警告信息给管理员时需提供本信息 -->
		<active.mode>prd</active.mode> <!-- 激活prd模式 -->
		<!-- 系统从session中获取用户的真实名称和账号ID 当异常发生时将此信息发送给管理员 -->
		<session-user-attribute-name>user</session-user-attribute-name> <!-- session中用户的attribute name -->
		<session-user-name-path>name</session-user-name-path> <!-- user的name属性名称 -->
		<session-user-id-path>id</session-user-id-path> <!-- user的id属性名称  -->
		<!-- 系统管理员，可接受邮件通知 -->
		<administrator id="1023" name="smith" email="smith@abc.com"></administrator>
		<!-- 系统审计员，可接受邮件通知 -->
		<auditor id="2189" name="frank" email="frank@abc.com"></auditor>
	</application>
	<!-- 用户可定义多个模式切换，在不同的情况下激活不同的模式即可 -->
	<mode id="prd"><!-- 生产环境安全配置 -->
		<exceptions>
			<!-- 设为false时不抓取异常 -->
			<capture>true</capture>
			<mailto>john@abc.com</mailto>
		</exceptions>
		<!-- 限额配置 -->
		<quotas>
			<max-sessions>10</max-sessions> <!-- 允许的session数量，超过即禁止使用系统 -->
			<max-sessions-per-user>1</max-sessions-per-user> <!-- 允许同一账号的登录数量，设为1即可禁止共享账号登录 -->
			<max-online-users>5</max-online-users> <!-- 最大在线用户配置  -->
			<max-same-url-open-per-session>30/10s</max-same-url-open-per-session>  <!-- 操作速度配置，10s内对同一个url的访问次数不得超过30次  -->
			<max-exceptions-per-url>5</max-exceptions-per-url>  <!-- 异常保护，若url出现500异常，则最多可以被访问5次  -->
			<out-of-service-redirect-url>http://127.0.0.1</out-of-service-redirect-url>  <!-- session和online users超限的转发页面  -->
		</quotas>
		<!-- 安全防护 -->
		<defences>
			<!-- enable cross site request forgery attack defence -->
			<csrf>true</csrf>
			<!-- enable CC attack defence -->
			<cc>true</cc>
			<!-- 输入验证，支持多模式匹配 -->
			<input-validator>
				<!-- 全局不验证的 name和url，如password属性不验证，支持正则配置 -->
				<except-names>global except names</except-names>
				<except-urls>global except urls</except-urls>
				<!-- 自定义模式1 -->
				<pattern>
					<name>SQL</name> <!-- 模式名称 -->
					<description>SQL Inject Detect</description> <!-- 模式描述 -->
					<expression> <!-- 模式规则，支持正则 -->
					<![CDATA[
					select|union|and|or|&&|from|dual|char\(
					]]></expression> 
					<except-names>password</except-names> <!-- 例外属性 -->
					<except-urls></except-urls> <!-- 例外URL -->
					<action>replace</action><!-- warn|intercept|replace -->
					<!-- WARN警告，intercept阻断，replace替换 -->
				</pattern>
				<pattern>
					<name>XSS</name>
					<description>XSS Attack Detect</description>
					<expression>
					<![CDATA[
					<script>|iframe|frame
					]]></expression>
					<except-names>password</except-names>
					<except-urls></except-urls>
					<action>replace</action>
				</pattern>
				<pattern>
					<name>dangerous-char</name>
					<description>Dangerous Char Detect</description>
					<expression>
					<![CDATA[
					@@|%|!
					]]></expression>
					<except-names>password</except-names>
					<except-urls></except-urls>
					<action>replace</action>
				</pattern>
			</input-validator>
		</defences>
	</mode>
	<!-- 开发环境安全配置 -->
	<mode id="dev">
		<exceptions>
			<capture>true</capture>
			<mailto>john@abc.com</mailto>
		</exceptions>
		<quotas>
			<max-sessions>10</max-sessions>
			<max-sessions-per-user>1</max-sessions-per-user>
			<max-online-users>5</max-online-users>
			<!-- user can open the same URL 30 times per session in 10 seconds,the default time unit is second-->
			<max-same-url-open-per-session>30/10s</max-same-url-open-per-session>
			<max-exceptions-per-url>5</max-exceptions-per-url>
			<out-of-service-redirect-url>http://127.0.0.1</out-of-service-redirect-url>
		</quotas>
		<defences>
			<csrf>enabled</csrf>
			<cc>enabled</cc>
			<input-validator>
				<except-names>global except names</except-names>
				<except-urls>global except urls</except-urls>
				<pattern>
					<name>SQL</name>
					<description>SQL Inject Detect</description>
					<expression><![CDATA[
					select|union|and|or|&&|from|dual|char\(
					]]></expression>
					<except-names>password</except-names>
					<except-urls></except-urls>
					<action>warn</action>
				</pattern>
				<pattern>
					<name>XSS</name>
					<description>XSS Attack Detect</description>
					<expression><![CDATA[
					<script>|iframe|frame
					]]></expression>
					<except-names></except-names>
					<except-urls></except-urls>
					<action>intercept</action>
				</pattern>
				<pattern>
					<name>dangerous-char</name>
					<description>Dangerous Char Detect</description>
					<expression><![CDATA[
					@@|%|!|=|<|>
					]]></expression>
					<except-names></except-names>
					<except-urls></except-urls>
					<action>replace</action>
				</pattern>
			</input-validator>
		</defences>
	</mode>
</waf>
```
 